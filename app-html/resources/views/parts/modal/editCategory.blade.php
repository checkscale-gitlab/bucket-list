<div class="modal fade" id="modal-editCategory-{{ $category->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Edit category</h5>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>

            <div class="modal-body">
                <form action="/category/update/{{ $category->id }}" id="modal-editCategory-{{ $category->id }}-form" method="post">
                    @csrf
                    @method('patch')
@if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
    @foreach ($error->editCategory->all() as $error)
                            <li>{{ $error }}</li>
    @endforeach
                    </ul>
                </div>
@endif
                    <div class="form-group row">
                        <label for="modal-editCategory-{{ $category->id }}-title" class="col-sm-4 col-form-label">Title</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="modal-editCategory-{{ $category->id }}-title" name="title" value="{{ $category->title }}">
                        </div>
                    </div>

                </form>

                <form action="/category/delete/{{ $category->id }}" id="deleteCategory-{{ $category->id }}-form" method="POST" >
                    @csrf
                    @method('delete')
                </form>

            </div>

            <div class="modal-footer">
                <button type="submit" form="deleteCategory-{{ $category->id }}-form" class="btn btn-danger">Delete</button>
                <button type="submit" form="modal-editCategory-{{ $category->id }}-form" class="btn btn-primary">Submit</button>
            </div>

        </div>
    </div>
</div>
