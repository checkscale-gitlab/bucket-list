@extends('layouts.app')

@section('content')
<div class="container">
    @include('parts.errors')
    <div class="row justify-content-center">
        <div class="col-md-4 py-4">
            <nav class="navbar flex-column align-items-start navbar-light">
                <button type="button" class="btn btn-primary" style="margin-bottom:1rem" data-toggle="modal" data-target="#modal-addItem">Add</button>
                @include('parts.modal.addItem')
                <ul class="navbar-nav flex-column">
                    <li class="nav-item {{ !$trash_box ? 'active' : '' }}"><a class="nav-link" href="/">All List</a></li>
                    <li class="nav-item">Categories
                        <ul class="nav flex-column" style="margin-left:1rem;width: 100%;">
                            <li class="nav-item"><a class="nav-link" href="#item-list">Uncategorized</a></li>
@foreach (Auth::user()->categories->sortBy('order') as $category)
                            <li class="nav-item d-flex justify-content-between align-items-center">
                                <a class="nav-link" href="#category-{{ $category->id }}-title">{{ $category->title }}</a>
                                <button type="button" class="btn btn-secondary btn-sm rounded-pill" style="color:#fff" data-toggle="modal" data-target="#modal-editCategory-{{ $category->id }}">edit</button>
                                @include('parts.modal.editCategory')
                            </li>
@endforeach
                            <li class="nav-item">
                                <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modal-addCategory">Add New Category</button>
                                @include('parts.modal.addCategory')
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item {{ $trash_box ? 'active' : '' }}"><a class="nav-link" href="/trash">Trash Box</a></li>
                </ul>
            </nav>
        </div>
        <div class="col-md-8 py-4" >
            <ul class="list-group" data-bs-spy="scroll" style="position: relative;overflow: auto;height:70vh">
                <li id="item-list" style="height:1px"></li>
@foreach (Auth::user()->items()->where('category_id', 0)->where('in_trash', $trash_box ?? '')->orderBy('order')->get() as $item)
                @include('parts.item')
@endforeach

@foreach (Auth::user()->categories->sortBy('order') as $category)
                <li id="category-{{ $category->id }}-title" href="#category-{{ $category->id }}" class="list-group-item list-group-item-primary lead">{{ $category->title }}</li>
    @foreach ($category->items()->where('in_trash', $trash_box ?? '')->orderBy('order')->get() as $item)
                @include('parts.item')
    @endforeach
@endforeach
            </ul>
        </div>
    </div>
</div>
@endsection
