/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!*********************************************!*\
  !*** ./resources/js/changeFinishedState.js ***!
  \*********************************************/
document.addEventListener('DOMContentLoaded', function () {
  var checkBoxElements = Array.from(document.getElementsByClassName('change-finished-state'));
  var CountElement = document.getElementById('total-count-of-finished-items');
  var csrfToken = document.queryselector('meta[name="csrf-token"]').attr('content');
  checkBoxElements.forEach(function (element) {
    var liElement = element.closest('li');
    element.addEventListener('change', function () {
      var beforeValue = Number(CountElement.textContent);

      if (this.checked) {
        CountElement.textContent = beforeValue + 1;
        liElement.classList.add('finished');
      } else {
        CountElement.textContent = beforeValue - 1;
        liElement.classList.remove('finished');
      }

      var xhr = new XMLHttpRequest();
      xhr.setRequestHeader('X-CSRF-TOKEN', csrfToken);
      xhr.responseType = 'json';

      xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            var jsonObject = JSON.parse(xhr.responseText);
            console.log(jsonObject.message);
          }
        }
      };

      xhr.open('POST', '/change-finished-state/' + this.dataset.id);
      xhr.send('finished=' + this.checked);
    }, {
      passive: true
    });
  });
});
/******/ })()
;