<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the items owned by the user.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany('App\Item');
    }

    /**
     * Get the Categories owned by the user.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Categories()
    {
        return $this->hasMany('App\Category');
    }

    /**
     * Get total number of items owned by the user.
     *
     * @param string $string Specify whether it is the total number ("total"),<br />
     * the number finished ("finished"), or the number not finished ("unfinished").<br />
     * Default value is "total".
     * @param int $max Specify the maximum output value.
     * @return int
     */
    public function itemsCount($string = 'total', $max = null)
    {
        $output = 0;
        if ($string === 'total') {
            $output = $this->items()->where('in_trash', 0)->count();
        } elseif ($string === 'finished') {
            $output = $this->items()->where('in_trash', 0)->where('finished', 1)->count();
        } elseif ($string === 'unfinished') {
            $output = $this->items()->where('in_trash', 0)->where('finished', 0)->count();
        }

        if (isset($max) && is_int($max)) {
            $output = min($max, $output);
        }

        return $output;
    }

    /**
     * Set the order of the items.
     *
     * @param boolean $seeder Whether to use it within Seeder or not.
     * Default value is false.
     * @return void
     */
    public function resortItemOrder($seeder = false)
    {
        $categories = $this->categories()->orderby('order');

        if ($categories->exists()) {
            $category_ids = array_merge([ 0 ], $categories->pluck("id")->all());
        } else {
            $category_ids = [ 0 ];
        }

        $order = 1;
        foreach ($category_ids as $category_id) {
            if ($seeder) {
                $items = $this->items()
                    ->saveMany(factory(Item::class, rand(10, 20))
                    ->make([ 'category_id' => (int) $category_id ]));
            } else {
                $items = $this->items()
                    ->where('category_id', $category_id)
                    ->where('in_trash', false)
                    ->orderby('order')
                    ->get();
            }
            foreach ($items as $item) {
                if (!$item->in_trash) {
                    $item->order = $order;
                    $item->save();
                    $order++;
                }
            }
        }
    }
}
